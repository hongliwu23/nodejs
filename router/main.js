module.exports = function(app)
{
	 //index page
     app.get('/',function(req,res){
        res.render('index.html')
     });

     //get method to read data and counter
     app.get('/getCounter',function(req,res){
        res.json({data: new Date(), counter : counter});
     });

     //post method to increase counter
     app.post('/addCounter', function(req,res){
     	counter = counter + 1;
     	res.json({data: new Date(), counter : counter});
     	console.log("The global counter is " + counter);
     });
}
