var express    =    require('express');
var app        =    express();

//counter - global varaible
global.counter = 0;

require('./router/main')(app);
app.set('views',__dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

var server     =    app.listen(8080,function(){
console.log("Express is running on port 8080");
console.log("The global counter is " + counter);
});